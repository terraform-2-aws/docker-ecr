AWS :cloud: Terraform  and CI/CD :fox:

**Intro**:

- At the initial stage the gitlab-ci creates a Docker image and store the flask API application on gitlab container. The stored address of this image is `registry.gitlab.com/terraform-2-aws/docker-ecr:latest`. In parallel with Docker creation the terraform init also runs to create the initial stage for running the terraform. When both of these stages are done terraform validate stage starts. 

![](pipeline.PNG)
- This stage preview changes before applying and all of the information stores in a json file. After the plan file is created the deploy stage starts. Both of these steps are set as manual to avoid unintentional resource creation. In below you can see the output of the API tests via postman. In `/terraform` folder you can find the necessary code which can create the whole infrastructure and `/web` is a folder which contains the API application written in python. This image is finally send into AWS ECS on Fargate. In this example two number of Fargates are running to balance the load. A load balancer with forwarding port is also considered to distribute the loads.

- The Structure for this approach is as below

![](Structure.png)

- The final result and API endpoint is also checked via postman

![](postman.png)
